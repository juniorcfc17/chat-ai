import NextAuth from "next-auth"
import GoogleProvider from "next-auth/providers/google";
import GitlabProvider from "next-auth/providers/gitlab";
import GitHubProvider from "next-auth/providers/github";

const handler = NextAuth({
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_CLIENT_ID as string,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET as string
    }),
    GitlabProvider({
      clientId: process.env.GITLAB_CLIENT_ID as string,
      clientSecret: process.env.GITLAB_CLIENT_SECRET as string
    }),
    GitHubProvider({
      clientId: process.env.GITHUB_ID as string,
      clientSecret: process.env.GITHUB_SECRET as string
    })
  ],

  callbacks: {
    async session({ session, token, user }) {
      return session
    }
  },

  session: {
    maxAge: 60 * 60 // 1 hour 
  },

  pages: {
    signIn: "/auth"
  },

  secret: process.env.NEXTAUTH_SECRET
})

export { handler as GET, handler as POST }
