'use client'

import { Loading } from "@/components/Loading";
import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";

export default function Home() {
  const { status } = useSession()
  const { push } = useRouter()

  if (status == 'unauthenticated') {
    new Promise((resolve) => setTimeout(resolve, 2000))
      .then(() => {
        push('/auth');
      })
  }

  if (status == 'authenticated') {
    new Promise((resolve) => setTimeout(resolve, 2000))
      .then(() => {
        push('/home');
      })
  }

  return (
    <div>
      <Loading />
    </div>
  );
}
