import { Skeleton } from "@/components/ui/skeleton";

export function SkeletonAuth() {
  return (
    <div className='min-h-screen grid grid-cols-2 antialiased'>
      <div className='h-full border-r border-foreground/5 bg-zinc-900 p-10 flex flex-col justify-between'>
        <div className='flex items-center gap-3'>
          <Skeleton className="w-10 h-10 rounded-full bg-slate-300/20" />
          <Skeleton className='w-36 h-6 bg-slate-300/20' />
        </div>
        <div className="w-full flex items-center justify-center overflow-hidden">
          <Skeleton className="w-8 h-8 rounded-md" />
        </div>
        <footer>
          <Skeleton className='w-64 h-4 bg-slate-300/20' />
        </footer>
      </div>

      <div className='flex flex-col items-center justify-center bg-slate-100 relative'>
        <div className="p-8">
          <div className="w-[350px] flex flex-col justify-center gap-6">
            <div className="flex flex-col items-center gap-2 text-center">
              <div className="flex items-center justify-center gap-2">
                <Skeleton className="w-10 h-10 rounded-md bg-slate-700/40" />
                <Skeleton className='w-36 h-6 bg-slate-700/40' />
              </div>
              <Skeleton className="w-64 h-4 mt-4 rounded-md bg-slate-700/20" />
            </div>

            <Skeleton className='w-full px-6 py-6 mt-2 bg-slate-600/80' />
            <Skeleton className='w-full px-6 py-6 bg-slate-600/80' />

          </div>
        </div>
      </div>
    </div>
  )
}