'use client'
import { HeroPattern } from "../../components/HeroPattern"
import { Bot } from 'lucide-react'
import { Google } from "../../components/Google";
import { GitHub } from "../../components/GitHub";
import { useSession } from "next-auth/react";
import { SkeletonAuth } from "./skeleton";
import { useRouter } from "next/navigation";
import { motion } from 'framer-motion'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { GitLab } from "@/components/GitLab";

export default function Auth() {
  const session = useSession()
  const router = useRouter()

  if (session.status === 'authenticated') {
    router.replace('/home');
  }

  return (
    <>
      {session.status == 'loading' ?
        <SkeletonAuth />
        :
        <div className='min-h-screen grid grid-cols-2 antialiased'>
          <div className='h-full border-r border-foreground/5 bg-zinc-900 p-10 text-muted-foreground flex flex-col justify-between'>
            <div className='flex items-end gap-3 text-lg text-foreground'>
              <motion.div initial={{ opacity: .5 }} animate={{ opacity: 1 }} transition={{ type: 'tween' }} >
                <motion.div
                  style={{ display: 'flex', alignItems: 'end' }}
                  animate={{
                    y: [-2, -12, -2, -7, -2, -5, -2, -2],
                  }}
                  transition={{
                    repeat: Infinity,
                    duration: 1.5,
                  }}
                >
                  <Bot className='h-8 w-8 text-slate-300' />
                </motion.div>
              </motion.div>
              <span className='font-bold text-xl text-slate-100'>CHAT.IA</span>
            </div>
            <div className="w-full flex items-center justify-center overflow-hidden">
              <HeroPattern />
            </div>
            <footer className='text-sm font-normal text-slate-100/80'>
              Desenvolvido por <a className="underline hover:text-slate-100" target="_blank" href="https://gitlab.com/juniorcfc17">Adilson Junior</a> - {new Date().getFullYear()}
            </footer>
          </div>

          <div className='flex flex-col items-center justify-center bg-slate-100 relative'>
            <div className="p-8">
              <div className="w-[350px] flex flex-col justify-center gap-6">
                <div className="flex flex-col gap-2 text-center">
                  <div className="flex justify-center gap-2">
                    <Bot className='h-7 w-7 text-zinc-800' />
                    <span className="text-2xl font-semibold tracking-tight">Chat IA</span>
                  </div>
                  <p className="text-sm text-muted-foreground">Selecione uma opção e comece a utilizar.</p>
                </div>

                <Google />
                <GitHub />
                <GitLab />

              </div>
            </div>
            <ToastContainer />
          </div>
        </div>
      }
    </>
  )
}