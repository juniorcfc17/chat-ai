'use client'

import { Chat } from "@/components/Chat";
import { Header } from "@/components/Header";
import { Loading } from "@/components/Loading";
import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { ToastContainer } from 'react-toastify'

export default function Home() {
  const { status } = useSession()
  const { push } = useRouter()
  const [isLoading] = useState(status === 'authenticated' ? false : true)

  if (status == 'unauthenticated') {
    new Promise((resolve) => setTimeout(resolve, 2000))
      .then(() => {
        push('/auth');
      })
  }
  return (
    <>
      {isLoading && <Loading />}

      {!isLoading &&
        <>
          <Header />
          <div className="flex items-center justify-center mt-10 bg-zinc-900">
            <Chat />
          </div>
          <ToastContainer />
        </>
      }
    </>
  )
}