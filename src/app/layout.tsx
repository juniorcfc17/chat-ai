import type { Metadata } from "next";
import { SpeedInsights } from "@vercel/speed-insights/next"
import { Analytics } from '@vercel/analytics/react';
import { Poppins } from "next/font/google";
import "./globals.css";
import NextAuthSessionProvider from "./providers/sessionProvider";

const poppins = Poppins({ subsets: ["latin"], weight: ["100", '400', '700'] });

export const metadata: Metadata = {
  title: "Chat.IA",
  description: "Chat integrado com API Openai",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="pt-br">
      <NextAuthSessionProvider>
        <body className="min-h-screen bg-zinc-900">
          {children}
          <SpeedInsights />
          <Analytics />
        </body>
      </NextAuthSessionProvider>
    </html>
  );
}
