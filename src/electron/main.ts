const { app, BrowserWindow } = require('electron');

function createWindow() {
  const win = new BrowserWindow({
    width: 1360,
    maxWidth: 1360,
    height: 960,
    maxHeight: 960,
    frame: true,
    show: true,
    autoHideMenuBar: false,
    webPreferences: {
      nodeIntegration: true,
    },
  });

  process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';

  win.loadURL('https://chat-ai-henna.vercel.app/')
  win.once('ready-to-show', () => {
    win.show();
  })
}

app.whenReady().then(createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
