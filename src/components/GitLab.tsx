'use client'

import { signIn } from 'next-auth/react';
import { useState } from 'react';
import gitLabLogo from "../../public/gitlab.svg"
import Image from 'next/image';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Button } from './ui/button';

export function GitLab() {
  const [isLoading, setIsLoading] = useState(false)

  function authGithub() {
    setIsLoading(true)
    toast.promise(new Promise(resolve => setTimeout(resolve, 2000)), { pending: 'Solicitando autorização...' }, {
      theme: 'dark',
      autoClose: 2000,
      closeOnClick: true,
      type: 'info',
      position: 'bottom-right',
    })
    setTimeout(() => {
      signIn('gitlab').catch(() => {
        setIsLoading(false)
        toast.error('Erro ao solicitar autenticação, tente novamente!', {
          theme: 'dark',
          autoClose: 2000,
          closeOnClick: true,
          type: 'error',
          position: 'bottom-right'
        })
      })
    }, 1500)
  }
  return (
    <>
      <Button disabled={isLoading} onClick={authGithub} className='w-full flex gap-14 items-center justify-start px-5 py-6'>
        <Image src={gitLabLogo} width={42} alt="Logo GitLab" priority />
        Entrar com GitLab
      </Button>
    </>
  )
}