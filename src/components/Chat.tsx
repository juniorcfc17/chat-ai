'use client'

import { useChat } from "ai/react";
import { Avatar, AvatarFallback, AvatarImage } from "./ui/avatar";
import { Button } from "./ui/button";
import { Card, CardContent, CardFooter } from "./ui/card";
import { ScrollArea } from "./ui/scroll-area";
import { HeroPatternChat } from "./HeroPatternChat";
import { Bot, Send } from "lucide-react";
import { useSession } from "next-auth/react";
import { Skeleton } from "./ui/skeleton";
import { Textarea } from "./ui/textarea";

export function Chat() {
  const { messages, input, handleInputChange, handleSubmit, isLoading, error, } = useChat()
  const { data } = useSession()

  return (
    <Card className="w-[720px] m-auto bg-zinc-950/90 border-none shadow-sm">
      <CardContent>
        <div className="absolute">
          <HeroPatternChat />
        </div>
        <ScrollArea className="h-[640px] w-full p-6 pt-8">
          {messages.map((message) => {
            return (
              <div key={message.id} className="flex gap-3 to-slate-600 mb-4 text-sm">
                {message.role === 'user' &&
                  <Avatar>
                    <AvatarFallback className="bg-transparent">
                      <Skeleton className="w-10 h-10 bg-slate-50/80" />
                    </AvatarFallback>
                    <AvatarImage src={data?.user?.image!} />
                  </Avatar>
                }
                {message.role === 'assistant' &&
                  <Avatar>
                    <Bot className='h-10 w-10 text-violet-900' />
                  </Avatar>
                }
                <p className="leading-relaxed">
                  <span className="block font-bold text-base text-slate-100">
                    {message.role === 'user' ? `${data?.user?.name}` : 'CHAT.IA'}
                  </span>
                  <p className="text-slate-500 text-base">
                    {message.content}
                    {!isLoading && error && error.message}
                  </p>
                </p>
              </div>
            )
          })}
        </ScrollArea>
      </CardContent>

      <CardFooter>
        <form onSubmit={handleSubmit} className="w-full flex">
          <Textarea
            placeholder="Faça sua pergunta..."
            className="max-w-[672px] max-h-52 h-16 resize-none pl-6 pr-14 text-base leading-relaxed placeholder:text-slate-300/70 text-slate-300 border-zinc-500 focus:outline-none focus:ring-2 focus:ring-emerald-300 snap-proximity"
            value={input}
            onChange={handleInputChange}
          />
          <Button type="submit" className="bg-zinc-900 px-5 rounded-full -ml-14 mt-2">
            <Send className="w-4 h-4 absolute" />
          </Button>
        </form>
      </CardFooter>
    </Card>
  )
}