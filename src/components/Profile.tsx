'use client'
import { signOut, useSession } from "next-auth/react"
import { Dialog } from "./ui/dialog"
import { DropdownMenu, DropdownMenuContent, DropdownMenuItem, DropdownMenuTrigger, DropdownMenuSeparator } from "./ui/dropdown-menu"
import { LogOut, Mail, User } from "lucide-react"
import { Avatar, AvatarFallback, AvatarImage } from "./ui/avatar"
import { Skeleton } from "./ui/skeleton"
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';

export function Profile() {
  const { data } = useSession()

  function handleSignOut() {
    toast.promise(new Promise(resolve => setTimeout(resolve, 1500)), {
      pending: 'Saindo...'
    }, {
      theme: 'dark',
      autoClose: 1500,
      closeOnClick: true,
      type: 'info',
      position: 'bottom-right',
    })
    new Promise((resolve) => setTimeout(resolve, 1500))
      .then(() => {
        signOut();
      })
  }

  return (
    <>
      <Dialog>
        <DropdownMenu>
          <DropdownMenuTrigger asChild>
            <Avatar className="w-11 h-11 border-2 border-zinc-400 hover:shadow-md hover:shadow-zinc-400 cursor-pointer transition ease-in-out">
              <AvatarFallback className="bg-transparent">
                <Skeleton className="w-10 h-10 bg-slate-50/80" />
              </AvatarFallback>
              <AvatarImage src={data?.user?.image!} />
            </Avatar>
          </DropdownMenuTrigger>

          <DropdownMenuContent align="end" className="w-56 bg-slate-800 text-slate-400">

            <DropdownMenuItem className="cursor-auto">
              <User className="w-5 h-5 mr-2" />
              <span className="ml-2 overflow-hidden text-ellipsis">{data?.user?.name}</span>
            </DropdownMenuItem>
            <DropdownMenuItem className="cursor-auto">
              <Mail className="w-5 h-5 mr-2" />
              <span className="ml-2 overflow-hidden text-ellipsis">{data?.user?.email}</span>
            </DropdownMenuItem>

            <DropdownMenuSeparator />

            <DropdownMenuItem asChild className="text-rose-500 dark:text-rose-400 cursor-pointer">
              <button className="w-full" onClick={handleSignOut}>
                <LogOut className="w-4 h-4 mr-2" />
                <span>Sair</span>
              </button>
            </DropdownMenuItem>
          </DropdownMenuContent>
        </DropdownMenu>
      </Dialog>
    </>
  )
}