'use client'

import { motion } from 'framer-motion'
import { Bot } from 'lucide-react'

export function Loading() {
  return (
    <div className="h-screen flex items-center justify-center">
      <motion.div initial={{ opacity: .5 }} animate={{ opacity: 1 }} transition={{ type: 'tween' }} >
        <motion.div
          style={{ display: 'flex', alignItems: 'end' }}
          animate={{
            y: [0, -10, 0],
          }}
          transition={{
            repeat: Infinity,
            duration: 1,
          }}
        >
          <Bot className='h-20 w-20 text-slate-300' />
        </motion.div>
      </motion.div>
    </div>
  )
}