import { Profile } from "./Profile";
import { Bot } from 'lucide-react'

export function Header() {
  return (
    <div className="h-16 flex items-center justify-between px-8 py-8 bg-zinc-900">
      <div className='flex items-end gap-3 text-lg text-foreground'>
        <Bot className='h-8 w-8 text-slate-300' />
        <span className='font-bold text-xl text-slate-100'>CHAT.IA</span>
      </div>
      <Profile />
    </div>
  )
}